package cl.ubb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.Date;
import java.util.List;

import cl.ubb.model.Automovil;
import cl.ubb.model.Reserva;

import cl.ubb.service.ReservaServices;

@RequestMapping("/reserva")
@RestController
public class ReservaController {
	
	@Autowired
	private ReservaServices reservaservices;
	
	@RequestMapping (value = "/registrarReserva", method = POST)
	@ResponseBody
	public ResponseEntity<Reserva>registrarReserva(@RequestBody Reserva reserva){
		reservaservices.crearReserva(reserva);
		return new ResponseEntity<Reserva>(reserva, HttpStatus.CREATED);
		
	}
	
	@RequestMapping (value = "/obtenerReservas/{fecha}/{idCliente}", method = GET)
	public ResponseEntity<List<Reserva>>obtenerReservas(@PathVariable("fecha") Date fecha, @PathVariable("idCliente") long idCliente){
		
		return new ResponseEntity<List<Reserva>>(reservaservices.obtenerReservasDeUnClienteDesdeUnaFecha(fecha, idCliente), HttpStatus.OK);
		
	}

}
