package cl.ubb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod.*;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;


import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;

import cl.ubb.model.Automovil;
import cl.ubb.service.AutomovilServices;

@RequestMapping("/auto")
@RestController
public class AutomovilController {
	
	@Autowired
	private AutomovilServices autoServices;
	
	@RequestMapping (value = "/obtenerAutos/{categoriaAuto}", method = GET)
	public ResponseEntity<List<Automovil>>obtenerAutos(@PathVariable("categoriaAuto") String categoriaAuto){
		
		return new ResponseEntity<List<Automovil>>(autoServices.obtenerPorCategoria(categoriaAuto), HttpStatus.OK);
		
	}
}
