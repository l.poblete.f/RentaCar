package cl.ubb.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


import cl.ubb.model.Cliente;
import cl.ubb.service.ClienteServices;

@RequestMapping("/cliente")
@RestController

public class ClienteController {
	
	@Autowired
	private ClienteServices clienteService;
	
	@RequestMapping (value = "/crearCliente", method = POST)
	@ResponseBody
	public ResponseEntity<Cliente>CrearCliente(@RequestBody Cliente cliente){
		clienteService.crearCliente(cliente);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.CREATED);
		
	}
	
	@RequestMapping (value = "/obtenerCliente/{rut}", method = GET)
	public ResponseEntity<Cliente>obtenerCliente(@PathVariable("rut") String rut){
		
		return new ResponseEntity<Cliente>(clienteService.obtenerClientePorRut(rut), HttpStatus.OK);
		
	}
	
	@RequestMapping (value = "/ListarClientes}", method = GET)
	public ResponseEntity<List<Cliente>>obtenerClientes(@RequestBody List<Cliente> cliente){
		
		return new ResponseEntity<List<Cliente>>(cliente, HttpStatus.OK);
		
	}

}
