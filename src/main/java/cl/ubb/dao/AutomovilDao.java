package cl.ubb.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Automovil;



public interface AutomovilDao extends CrudRepository<Automovil,Long>{
	
	@Query("select a from Automovil a where categoria = ?1")
	public List<Automovil> findByCategoria(String categoria);
}
