package cl.ubb.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Cliente;


public interface ClienteDao extends CrudRepository<Cliente,Long>{
	
	@Query("select t from Cliente t where rut = ?178430564")
	Cliente findClienteByRut(String anyString);

}
