package cl.ubb.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import cl.ubb.model.Reserva;



public interface ReservaDao extends CrudRepository<Reserva,Long>{
	
	@Query("select r from Reserva r where fecha_inicio > ?1 and id_cliente = ?2")
	public List<Reserva> findByStartDateAfterAndIdCliente(Date date, long id);

}
