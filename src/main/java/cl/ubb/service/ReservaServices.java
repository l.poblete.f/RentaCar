package cl.ubb.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Reserva;

public class ReservaServices {
	
	private ReservaDao reservadao;
	
	@Autowired
	public ReservaServices(ReservaDao reservadao){
		this.reservadao=reservadao;
	}
	
	public Reserva crearReserva(Reserva reserva) {
		return reservadao.save(reserva);
	}

	public List<Reserva> obtenerReservasDeUnClienteDesdeUnaFecha(Date fecha, long l) {
		// TODO Auto-generated method stub
		List<Reserva> reserva=new ArrayList<Reserva>();
		reserva=reservadao.findByStartDateAfterAndIdCliente(fecha,l);
		
		return reserva;
	}

}
