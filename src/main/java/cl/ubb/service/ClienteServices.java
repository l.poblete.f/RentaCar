package cl.ubb.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import cl.ubb.dao.ClienteDao;
import cl.ubb.model.Cliente;


public class ClienteServices {
	
	private ClienteDao clientedao;
	
	@Autowired
	public ClienteServices(ClienteDao clientedao){
		this.clientedao=clientedao;
	}
	
	public Cliente crearCliente(Cliente cliente) {
		return clientedao.save(cliente);
	}
	
	public List<Cliente> obtenerCliente() {
		List<Cliente> cliente = new ArrayList<Cliente>();
		return cliente;
	}
	
	public class noExisteClienteException extends RuntimeException{}
	
	public Cliente obtenerClientePorRut(String rut) {
		// TODO Auto-generated method stub
		 Cliente cliente = new Cliente();
		
		 cliente = clientedao.findClienteByRut(rut);
		
		 if (cliente == null){
			 throw new noExisteClienteException();
		 }else{
			 return cliente; 
		 }
	}

}
