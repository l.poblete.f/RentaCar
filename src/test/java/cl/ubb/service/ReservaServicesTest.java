package cl.ubb.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyLong;

import cl.ubb.dao.ReservaDao;
import cl.ubb.model.Reserva;

@RunWith(MockitoJUnitRunner.class)
public class ReservaServicesTest {

	@Mock
	private ReservaDao reservadao;
		
	@InjectMocks
	private ReservaServices reservaservice;
	
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void guardarReservaYRetornarla() {
		
		Reserva reserva=new Reserva();
		Reserva reserva2=new Reserva();
		
		when(reservadao.save(reserva)).thenReturn(reserva);
		reserva2=reservaservice.crearReserva(reserva);
		
		Assert.assertNotNull(reserva2);
		Assert.assertEquals(reserva, reserva2);
			
	}
	
	@Test
	public void listarReservasDeUnClienteDesdeUnaFecha() {
		
		List<Reserva> reserva=new ArrayList<Reserva>();
		List<Reserva> reserva2=new ArrayList<Reserva>();
		
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		String strFecha = "2015-12-25";
		Date fecha = null;
		
		try {
			fecha = formato.parse(strFecha);
			when(reservadao.findByStartDateAfterAndIdCliente(fecha,1L)).thenReturn(reserva);
			reserva2 = reservaservice.obtenerReservasDeUnClienteDesdeUnaFecha(fecha,1L);
			
		} catch (ParseException ex) {

		ex.printStackTrace();

		}
		
					
		//assert
		Assert.assertNotNull(reserva2);
		Assert.assertEquals(reserva, reserva2);
	}

}
