package cl.ubb.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.anyLong;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyString;



import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

import cl.ubb.model.Automovil;
import cl.ubb.service.AutomovilServices;


@RunWith(MockitoJUnitRunner.class)
public class AutomovilControllerTest {
	
	@Mock
	private AutomovilServices autoServices;
	
	@InjectMocks
	private AutomovilController autoController;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void listarAutosPorCategoria() {
		RestAssuredMockMvc.standaloneSetup(autoController);
		
		List<Automovil> lista=new ArrayList<Automovil>();
		Automovil auto=new Automovil();
		auto.setId(1L);
		auto.setMarca("kia");
		auto.setModelo("cerato");
		auto.setTransmision("automatico");
		auto.setCategoria("lujo");
		lista.add(auto);
		
		Mockito.when(autoServices.obtenerPorCategoria(anyString())).thenReturn(lista);
		
		given().
			contentType("application/json").
		when().
			get("/auto/obtenerAutos/{categoriaAuto}","lujo").
		then().
			body("categoria",is("lujo")).
			statusCode(200);
		
		
		
		
	}

}
