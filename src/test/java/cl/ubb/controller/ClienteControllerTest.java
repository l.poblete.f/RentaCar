package cl.ubb.controller;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.anyLong;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.anyList;

import java.util.ArrayList;
import java.util.List;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;


import cl.ubb.model.Cliente;
import cl.ubb.service.ClienteServices;

@RunWith(MockitoJUnitRunner.class)
public class ClienteControllerTest {
	
	@Mock
	private ClienteServices clienteService;
	
	@InjectMocks
	private ClienteController clienteController;
	
	@Test
	public void crearCliente() {
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setRut("178430564");
		cliente.setNombre("Andres");
		cliente.setCelular("98765432");
		cliente.setEmail("cliente@user.cl");

		
		Mockito.when(clienteService.crearCliente(Matchers.any(Cliente.class))).thenReturn(cliente);
		
		given().
			contentType("application/json").
			body(cliente).
		when().
			post("/cliente/crearCliente").
		then().
			body("nombre", equalTo(cliente.getNombre())).
			statusCode(201);
	}
	
	@Test
	public void obtenerUnCliente(){
		RestAssuredMockMvc.standaloneSetup(clienteController);
		Cliente cliente = new Cliente();
		cliente.setId(1L);
		cliente.setRut("178430564");
		cliente.setNombre("Andres");
		cliente.setCelular("98765432");
		cliente.setEmail("cliente@user.cl");
		
		Mockito.when(clienteService.obtenerClientePorRut(anyString())).thenReturn(cliente);
		
		given().
			contentType("application/json").
		when().
			get("/cliente/obtenerCliente{rut}","178430564").
		then().
			body("rut", is("178430564")).
			statusCode(200);
	}
	
	@Test
	public void listarClientes() {
		RestAssuredMockMvc.standaloneSetup(clienteController);
		
		List<Cliente> lista=new ArrayList<Cliente>();
		Cliente cliente=new Cliente();
		cliente.setId(1L);
		cliente.setRut("178430564");
		cliente.setNombre("Andres");
		cliente.setCelular("98765432");
		cliente.setEmail("cliente@user.cl");
		lista.add(cliente);
		
		Mockito.when(clienteService.obtenerCliente()).thenReturn(lista);
		
		given().
			contentType("application/json").
		when().
			get("/cliente/obtenerClientes").
		then().
			body("rut", equalTo(cliente.getRut())).
			statusCode(200);
		
		
		
		
	}
	
	
	
	
	
	

}
