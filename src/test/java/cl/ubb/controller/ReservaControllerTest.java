package cl.ubb.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.anyLong;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.equalTo;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;

import static com.jayway.restassured.module.mockmvc.RestAssuredMockMvc.given;

import cl.ubb.model.Reserva;

import cl.ubb.service.ReservaServices;


@RunWith(MockitoJUnitRunner.class)
public class ReservaControllerTest {
	
	@Mock
	private ReservaServices reservaservices;
	
	@InjectMocks
	private ReservaController reservacontroller;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void reservarAuto(){
		
		RestAssuredMockMvc.standaloneSetup(reservacontroller);
		Reserva reserva = new Reserva();
		reserva.setId_cliente(1L);
		reserva.setId_automovil(1L);
		
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		String strFecha = "2016-12-20";
		Date fecha = null;
		
		String strFecha2 = "2016-12-25";
		Date fecha2 = null;
		
		try {
			fecha = formato.parse(strFecha);
			fecha2 = formato.parse(strFecha2);
									
		} catch (ParseException ex) {

		ex.printStackTrace();

		}
		
		reserva.setFecha_inicio(fecha);
		reserva.setFecha_fin(fecha2);
		
		Mockito.when(reservaservices.crearReserva(Matchers.any(Reserva.class))).thenReturn(reserva);
		
		given().
			contentType("application/json").
			body(reserva).
		when().
			post("/reserva/registrarReserva").
		then().
			body("id_cliente", equalTo(1)).
			body("id_automovil", equalTo(1)).
			body("fecha_inicio", equalTo(reserva.getFecha_inicio())).
			body("fecha_fin", equalTo(reserva.getFecha_fin())).
			statusCode(201);
	}
	
	@Test
	public void listarReservasDeUnClienteDesdeUnaFecha() {
		
		RestAssuredMockMvc.standaloneSetup(reservacontroller);
		List<Reserva> lista=new ArrayList<Reserva>();
		Reserva reserva=new Reserva();
		reserva.setId_cliente(1L);
		reserva.setId_automovil(1L);
		
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		String strFecha = "2015-12-25";
		Date fecha = null;
		
		String strFecha2 = "2016-01-25";
		Date fecha2 = null;
		
		String strFecha3 = "2016-01-30";
		Date fecha3 = null;
		
		try {
			fecha = formato.parse(strFecha);
			fecha2 = formato.parse(strFecha2);
			fecha3 = formato.parse(strFecha3);
						
		} catch (ParseException ex) {

		ex.printStackTrace();

		}
		
		reserva.setFecha_inicio(fecha2);
		reserva.setFecha_fin(fecha3);
		
		lista.add(reserva);
		
		//Mockito.when(reservaservices.crearReserva(Matchers.any(Reserva.class))).thenReturn(reserva);
		
		Mockito.when(reservaservices.obtenerReservasDeUnClienteDesdeUnaFecha(fecha,1L)).thenReturn(lista);
		
		given().
			contentType("application/json").
		when().
			get("/reserva/obtenerReservas/{fecha}/{idCliente}",fecha,1).
		then().
			body("id_automovil",is(1)).
			body("fecha_inicio", equalTo(reserva.getFecha_inicio())).
			body("fecha_fin", equalTo(reserva.getFecha_fin())).
			statusCode(200);
		
	}

}
